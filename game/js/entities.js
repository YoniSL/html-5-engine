var aniPos = { x : 0, y : 0 };
var playerImage = new Image();
var walking = false;
var playerAni;
var playerState = 'idle_s';
var nextState = 1;

playerImage.src = 'resources/entities/player.png';

function drawPlayer() {
	entitiesContext.clearRect(0, 0, entities.width, entities.height);
	entitiesContext.drawImage(playerImage,
		(playerAni[playerState] !== undefined && playerAni[playerState].x 		!== undefined ? playerAni[playerState].x 		: playerAni.default.x),
		(playerAni[playerState] !== undefined && playerAni[playerState].y 		!== undefined ? playerAni[playerState].y 		: playerAni.default.y),
		(playerAni[playerState] !== undefined && playerAni[playerState].width 	!== undefined ? playerAni[playerState].width 	: playerAni.default.width),
		(playerAni[playerState] !== undefined && playerAni[playerState].height 	!== undefined ? playerAni[playerState].height 	: playerAni.default.height),
		(playerAni[playerState] !== undefined && playerAni[playerState].pos_x 	!== undefined ? playerAni[playerState].pos_x 	: playerAni.default.pos_x) + (tileSize*6),
		(playerAni[playerState] !== undefined && playerAni[playerState].pos_y 	!== undefined ? playerAni[playerState].pos_y 	: playerAni.default.pos_y) + (tileSize*4),
		(playerAni[playerState] !== undefined && playerAni[playerState].width 	!== undefined ? playerAni[playerState].width 	: playerAni.default.width),
		(playerAni[playerState] !== undefined && playerAni[playerState].height 	!== undefined ? playerAni[playerState].height 	: playerAni.default.height)
	);
}

function walk(direction) {

	if(!walking && direction != '') {
		walking				= true;

		var pos 			= direction == 'n' || direction == 's' ? 'y' : 'x';
		var cpos 			= direction == 's' || direction == 'e' ? -3  :   3;
		var aniState		= 0;
		var nextPos 		= { x : currentOffset.x + 6 + (pos == 'x' ? cpos > 0 ? -1 : 1 : 0), y : currentOffset.y + 4 + (pos == 'y' ? cpos > 0 ? -1 : 1 : 0) };
		var obstructed		= map.map[nextPos.y] !== undefined && map.map[nextPos.y][nextPos.x] !== undefined && map.map[nextPos.y][nextPos.x].walkable !== undefined ?
							 !map.map[nextPos.y][nextPos.x].walkable : false;



		playerState = "walk_" + direction + "_" + nextState;


		var walkInterval 	= setInterval(function() {
			if(!obstructed) aniPos[pos]	+=	cpos;
			else aniState	+= cpos;

			if(Math.abs(aniPos[pos]) >= tileSize / 3 * 2)
				playerState = "idle_" + direction;

			if(Math.abs(aniPos[pos]) >= tileSize -1 || Math.abs(aniState) >= tileSize -1 ) {
				clearInterval(walkInterval);
				aniPos.x = 	0;
				aniPos.y = 	0;
				if(!obstructed) currentOffset[pos]	+= cpos < 0 ? 1 : -1;
				walking  = false;
				playerState = "idle_" + direction;
				nextState = nextState == 1 ? 2 : 1;
			}
		}, 17 * (keys[16] !== undefined && keys[16] ? runningSpeed : movementSpeed));
	}
}

function getAni(name) {
	var aniObject = {}; 
	var aniRequest = new XMLHttpRequest();
	aniRequest.open('get', 'resources/entities/ani/'+name+'.json', false);
	aniRequest.onreadystatechange = function () {
		if(aniRequest.readyState == 4 && aniRequest.status == 200) {
			aniObject = JSON.parse(aniRequest.responseText);
			if(aniObject.default.x 		=== undefined) 	aniObject.default.x 		= 0;
			if(aniObject.default.y 		=== undefined) 	aniObject.default.y 		= 0;
			if(aniObject.default.width 	=== undefined) 	aniObject.default.width 	= 0;
			if(aniObject.default.height === undefined)	aniObject.default.height 	= 0;
			if(aniObject.default.pos_x 	=== undefined) 	aniObject.default.pos_x 	= 0;
			if(aniObject.default.pos_y 	=== undefined) 	aniObject.default.pos_y 	= 0;
		}
	};
	aniRequest.send(null);
	return aniObject;
}