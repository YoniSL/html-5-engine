var backgroundDrawInterval;
var playerDrawInterval;
var walkDrawInterval;
var background;
var entities;
var backgroundContext;
var entitiesContext;
var currentFps;
var frame = 0;
var frameTime;
var mapId = 0;
var tileSize = 16; //changed upon initialization
var currentOffset = { x : 0, y : 0};
var direction = '';
var keyInterval;
var keys = Array();


function initCore() {
	var success;

	background 	= document.getElementById('background');
	entities 	= document.getElementById('entities');

	if(background && entities) {
		backgroundContext 	= background.getContext('2d');
		entitiesContext		= entities.getContext('2d');

		if(backgroundContext && entitiesContext) {

			changeTextureQuality(quality);
			success = true;
		}
	}

	if(success) {

		frameTime 	= Date.now();
		map 		= getMap(mapId);
		playerAni	= getAni('player');

		backgroundDrawInterval 	= setInterval(backgroundDraw, 1000 / fps);
		playerDrawInterval 		= setInterval(drawPlayer, 1000 / fps);
		walkDrawInterval 		= setInterval(function() { walk(direction); }, 1);

		document.onkeydown = function(event) {
			keys[event.which] = true;
			
			switch(event.which) {
				case 87: 	// W
					direction = 'n';
					break;
				case 65: 	// A
					direction = 'w';
					break;
				case 83: 	// S
					direction = 's';
					break;
				case 68: 	// D
					direction = 'e';
					break;
			}
		}

		document.onkeyup = function(event) {
			keys[event.which] = undefined;

			switch(event.which) {
				case 87: 	// W
				case 65: 	// A
				case 83: 	// S
				case 68: 	// D
					if(keys[87] == undefined && keys[65] == undefined && keys[83] == undefined && keys[68] == undefined) {
						direction = '';
					} else {
						if(keys[87] != undefined) direction = 'n';
						if(keys[65] != undefined) direction = 'w';
						if(keys[83] != undefined) direction = 's';
						if(keys[68] != undefined) direction = 'e';
					}
					break;
			}
		}
	}
}

function backgroundDraw() {
	drawMap();
	frame++;
	if(frame == fps) {
		calcFps();
		document.title = "PePeWorld ("+currentFps+" fps)";
		frame = 0;
		frameTime = Date.now();
	}
}

function calcFps() {
	currentFps = Math.round((Date.now() - frameTime) / 1000 * fps);
}

function stop() {
	clearInterval(backgroundDrawInterval);
	clearInterval(playerDrawInterval);
	clearInterval(walkDrawInterval);
}

function changeTextureQuality(quality) { // needs scaling reset to work properly
	switch(quality) {
		case "low":
		case "16":
		case 16:
			resetScale();
			backgroundContext.scale(3, 3);
			palette.src = 'resources/palettes/default.bmp';
			tileSize 	= 16;
			break
		case "high":
		case "48":
		case 48:
			resetScale();
			backgroundContext.scale(1, 1);
			palette.src = 'resources/palettes/default48.bmp';
			tileSize 	= 48;
			break;
		default:
			break;
	}
}

function resetScale() { // workaround / hack for now
	background.width++;
	background.width--;
}