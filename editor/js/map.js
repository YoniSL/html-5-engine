var palette = new Image();
var map;
palette.src = 'resources/palettes/default48.bmp'

function drawMap() {
	for(var y = 0; y < background.height / tileSize; y++) {
		for(var x = 0; x < background.width / tileSize; x++) {
			backgroundContext.drawImage(palette,
				(map.map[y + currentOffset.y] != undefined && map.map[y + currentOffset.y][x + currentOffset.x] != undefined && map.map[y + currentOffset.y][x + currentOffset.x].x != undefined ? map.map[y + currentOffset.y][x + currentOffset.x].x : map.info.default_x) * tileSize,
				(map.map[y + currentOffset.y] != undefined && map.map[y + currentOffset.y][x + currentOffset.x] != undefined && map.map[y + currentOffset.y][x + currentOffset.x].y != undefined ? map.map[y + currentOffset.y][x + currentOffset.x].y : map.info.default_y) * tileSize,
				tileSize,		//unstretched palette width
				tileSize,		//unstretched palette height
				x * tileSize,	//pos x on canvas
				y * tileSize,	//pos y on canvas
				tileSize,		//size x (stretch)
				tileSize		//size y (stretch)
			);
		}
	}
}

function getMap(id) {
	var mapObject = {}; 
	var mapRequest = new XMLHttpRequest();
	mapRequest.open('get', 'resources/maps/map_'+id+'.json', false);
	mapRequest.onreadystatechange = function () {
		if(mapRequest.readyState == 4 && mapRequest.status == 200) {
			mapObject = JSON.parse(mapRequest.responseText);	
		}
	};
	mapRequest.send(null);
	return mapObject;
}