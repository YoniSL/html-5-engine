var backgroundDrawInterval;
var backgroundContext;
var toolboxContext;
var currentFps;
var background;
var frameTime;
var toolbox;
var frame			=	0;
var mapId			=	0;
var tileSize		=	16;
var currentOffset	=	{ x : 0, y : 0};
var block			=	{ x : -1, y : -1};
var pressed			=	false;
var shiftDown		=	false;
var shift			=	{ state : 0, old_x : -1, old_y : -1, x : -1, y : -1};


function initCore() {
	var success = false;

	background 	= document.getElementById('background');
	toolbox 	= document.getElementById('palette');

	if(background && toolbox) {
		backgroundContext 	= background.getContext('2d');
		toolboxContext 		= toolbox.getContext('2d');

		if(backgroundContext && toolboxContext) {
			changeTextureQuality(quality);
			success = true;
		}
	}

	if(success) {
		//frameTime = Date.now();
		//map = getMap(mapId);
		//backgroundDrawInterval = setInterval(backgroundDraw, 1000 / fps);

		toolboxContext.drawImage(palette,0,0);
		toolboxContext.lineWidth = 3;
		toolboxContext.strokeStyle = 'red';

		background.focus();

		background.onkeypress = function(event) {
			switch(event.which) {
				case 119: 	// W
					currentOffset.y -= 1;
					break;

				case 97: 	// A
					currentOffset.x -= 1;
					break;

				case 115: 	// S
					currentOffset.y += 1;
					break;

				case 100: 	// D
					currentOffset.x += 1;
					break;

			}
		}

		toolbox.onclick = function(e) {
			block.x = Math.floor(e.layerX / tileSize);
			block.y = Math.floor(e.layerY / tileSize);

			toolboxContext.drawImage(palette, 0, 0);
			toolboxContext.strokeRect(block.x * tileSize, block.y * tileSize, 48, 48);
		}

		background.onclick = function(e) {
			if(block.x >= 0) {
				backgroundContext.drawImage(palette,
					block.x * tileSize,
					block.y * tileSize,
					tileSize,
					tileSize,
					Math.floor(e.layerX / tileSize) * tileSize,
					Math.floor(e.layerY / tileSize) * tileSize,
					tileSize,
					tileSize
				);
			}
		}

		background.onmousemove = function(e) {
			if(block.x >= 0) {
				if(pressed) {
					if(shiftDown) {
						if(shift.state == 0) { shift.old_x = e.layerX; shift.old_y = e.layerY; }

						if(shift.state == 1) {
							if(Math.floor(shift.old_x / tileSize) != Math.floor(e.layerX / tileSize)) {
								shift.y = shift.old_y;
								state = 2;
							} else if(Math.floor(shift.old_y / tileSize) != Math.floor(e.layerY / tileSize)) {
								shift.x = shift.old_x;
								state = 2;
							}
						}

						backgroundContext.drawImage(palette,
							block.x * tileSize,
							block.y * tileSize,
							tileSize,
							tileSize,
							Math.floor((shift.x != -1 ? shift.x : e.layerX) / tileSize) * tileSize,
							Math.floor((shift.y != -1 ? shift.y : e.layerY) / tileSize) * tileSize,
							tileSize,
							tileSize
						);

						if(shift.state == 0) shift.state = 1;

					} else {
						backgroundContext.drawImage(palette,
							block.x * tileSize,
							block.y * tileSize,
							tileSize,
							tileSize,
							Math.floor(e.layerX / tileSize) * tileSize,
							Math.floor(e.layerY / tileSize) * tileSize,
							tileSize,
							tileSize
						);
					}
				}
			}
		}

		background.onmousedown = function(e) {
			pressed = true;			
		}

		document.onmouseup = function(e) {
			pressed = false;
		}

		document.onkeydown = function(e) {
			if(e.shiftKey) shiftDown = true;
		}

		document.onkeyup = function(e) {
			if(!e.shiftKey) {
				shiftDown = false;
				shift.state = 0;
				shift.old_y = -1;
				shift.old_x = -1;
				shift.x = -1;
				shift.y = -1;
			}
		}
	}
}

function backgroundDraw() {
	drawMap();
	frame++;
	if(frame == fps) {
		calcFps();
		document.title = "PePeWorld ("+currentFps+" fps)";
		frame = 0;
		frameTime = Date.now();
	}
}

function calcFps() {
	currentFps = Math.round((Date.now() - frameTime) / 1000 * fps);
}

function stop() {
	clearInterval(backgroundDrawInterval);
}

function changeTextureQuality(quality) {
	switch(quality) {
		case "low":
		case "16":
		case 16:
			resetScale();
			backgroundContext.scale(3, 3);
			palette.src = 'resources/palettes/default.bmp';
			tileSize 	= 16;
			break;
		case "high":
		case "48":
		case 48:
			resetScale();
			backgroundContext.scale(1, 1);
			palette.src = 'resources/palettes/default48.bmp';
			tileSize 	= 48;
			break;
		default:
			break;
	}
}

function resetScale() { // workaround / hack for now
	background.width++;
	background.width--;
}